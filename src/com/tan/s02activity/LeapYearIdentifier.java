package com.tan.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Input a year to see if it's a Leap Year: \n");

        int year = Integer.parseInt(input.nextLine());

        boolean leap = false;

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    leap = true;
                }
                else {
                    leap = false;
                }
            }
        }

        else {
            leap = true;
        }

        if (leap)
            System.out.println(year + " is a leap year! Cool.");
        else
            System.out.println(year + " is not a leap year! Too bad.");
} }
